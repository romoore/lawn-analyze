#!/usr/bin/perl

# Header info
# Local-IP Remote-IP Protocol Local-Port Remote-Port In-Bytes Out-Bytes In-Pkts Out-Pkts 1st-Pkt-Time Last-Pkt-Time 1st-Talk Last-Talk

# Good practice
use strict;
use warnings;

# Cross-platform path handling
use Path::Class;
# MRTG Record class
use Record;
# Fail if any error occurs
use autodie;

# Check number of arguments. Each one is a filename
if($#ARGV < 0) {
  print "Please provide at least one file to parse.\n";
  exit;
}

my $inputFile = file($ARGV[0]);
my $inputHandle = $inputFile->open("r") or die "Can't open $ARGV[0] for reading.";

my %bytesSent = ();
my @records = ();

while(my $line = $inputHandle->getline()){
  chomp($line);
  my @splat = split(/\s+/,$line);
  my $record = Record->new();
  $record->fromLine(@splat);
  push(@records,$record);
  my $currSent = $bytesSent{$record->localIp};
  if(not defined($currSent)) {
    $currSent = 0;
  }
  $bytesSent{$record->localIp} = ($currSent + $record->bytesOut);
}

foreach my $ip (sort keys %bytesSent) {
  printf("%s: %d\n", $ip, $bytesSent{$ip});
}

my @sortedStart = sort { $a->firstPacketTime cmp $b->firstPacketTime } @records;
foreach my $rec (@sortedStart) {
  print $rec . "\n";
}
$inputHandle->close();
