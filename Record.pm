#!/usr/bin/perl

# The name of this class
package Record;
# Good practice
use strict;
use warnings;

# Parsing timestamps
use Time::ParseDate;


# Constructor. Nothing is defined here.
sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{IP_LOCAL} = undef;
  $self->{IP_REMOTE} = undef;
  $self->{PROTOCOL} = undef;
  $self->{PORT_LOCAL} = undef;
  $self->{PORT_REMOTE} = undef;
  $self->{BYTES_IN} = undef;
  $self->{BYTES_OUT} = undef;
  $self->{PKTS_IN} = undef;
  $self->{PKTS_OUT} = undef;
  $self->{PKT_TIME_FIRST} = undef;
  $self->{PKT_TIME_LAST} = undef;
  $self->{TALKER_FIRST} = undef;
  $self->{TALKER_LAST} = undef;
  bless($self, $class);
  return $self;
}

# Destructor
sub DESTROY {
  # Does nothing right now
}
use overload ( '""'  => \&stringify );
sub stringify {
  my $self = shift;
  return sprintf ("L: %s:%d R: %s:%d Pr: %s %d B sent %d B received %d pkts sent %d pkts received %s-%s First Talker: %s Last Talker: %s", 
          $self->{IP_LOCAL},
          $self->{PORT_LOCAL},
          $self->{IP_REMOTE},
          $self->{PORT_REMOTE},
          protoString($self->{PROTOCOL}),
          $self->{BYTES_IN},
          $self->{BYTES_OUT},
          $self->{PKTS_IN},
          $self->{PKTS_OUT},
          $self->{PKT_TIME_FIRST},
          $self->{PKT_TIME_LAST},
          localOrRemote($self->{TALKER_FIRST}),
          localOrRemote($self->{TALKER_LAST}));
}

sub protoString {
	my $val = shift;
	if($val == 1) {
		return 'ICMP';
	}elsif ($val == 6) {
		return 'TCP';
	}elsif ($val == 17) {
		return 'UDP'
	}
	return $val;
}

sub localOrRemote {
  my $val = shift;
  if($val == 1){
    return "local";
  }
  return "remote";
}

# Sets or gets the local IP address for this record
sub localIp {
  my $self = shift;
  if(@_) {
    $self->{IP_LOCAL} = shift;
  }
  return $self->{IP_LOCAL};
}

# Sets or gets the remtoe IP address for this record
sub remoteIp {
  my $self = shift;
  if(@_) {
    $self->{IP_REMOTE} = shift;
  }
  return $self->{IP_REMOTE};
}

# Sets or gets the protocol value
sub protocol {
  my $self = shift;
  if(@_) {
    $self->{PROTOCOL} = shift;
  }

  return $self->{PROTOCOL};
}

# Sets or gets local port
sub localPort {
  my $self = shift;
  if(@_) {
    $self->{PORT_LOCAL} = shift;
  }
  return $self->{PORT_LOCAL};
}

# Sets or gets the remote port
sub remotePort {
  my $self = shift;
  if(@_) {
    $self->{PORT_REMOTE} = shift;
  }
  return $self->{PORT_REMOTE};
}

# Sets or gets the number of inbound bytes
sub bytesIn {
  my $self = shift;
  if(@_) {
    $self->{BYTES_IN} = shift;
  }

  return $self->{BYTES_IN};
}

# Sets or gets the number of outbound bytes
sub bytesOut {
  my $self = shift;
  if(@_) {
    $self->{BYTES_OUT} = shift;
  }
  return $self->{BYTES_OUT};
}

# Sets or gets the number of inbound packets 
sub packetsIn {
  my $self = shift;
  if(@_) {
    $self->{PKTS_IN} = shift;
  }
  return $self->{PKTS_IN};
}

# Sets or gets the number of outbound packets 
sub packetsOut {
  my $self = shift;
  if(@_) {
    $self->{PKTS_OUT} = shift;
  }
  return $self->{PKTS_OUT};
}

# Sets or gets the time of the first packet
sub firstPacketTime {
  my $self = shift;
  if(@_) {
    $self->{PKT_TIME_FIRST} = parsedate(shift);
  }
  return $self->{PKT_TIME_FIRST};
}

# Sets or gets the time of the last packet
sub lastPacketTime {
  my $self = shift;
  if(@_) {
    $self->{PKT_TIME_LAST} = parsedate(shift);
  }
  return $self->{PKT_TIME_LAST};
}

# Sets or gets the identity (local/remote) of the first "talker"
sub firstTalker {
  my $self = shift;
  if(@_) {
    $self->{TALKER_FIRST} = shift;
  }
  return $self->{TALKER_FIRST};
}

# Sets or gets the identity (local/remote) of the last "talker"
sub lastTalker {
  my $self = shift;
  if(@_) {
    $self->{TALKER_LAST} = shift;
  }
  return $self->{TALKER_LAST};
}

# Assign this object's values based on a file line.
sub fromLine {
  my $self = shift;
  if(@_){
    ( $self->{IP_LOCAL},
      $self->{IP_REMOTE},
      $self->{PROTOCOL},
      $self->{PORT_LOCAL},
      $self->{PORT_REMOTE},
      $self->{BYTES_IN},
      $self->{BYTES_OUT},
      $self->{PKTS_IN},
      $self->{PKTS_OUT},
      $self->{PKT_TIME_FIRST},
      $self->{PKT_TIME_LAST},
      $self->{TALKER_FIRST},
      $self->{TALKER_LAST}) = @_;

      $self->{PKT_TIME_FIRST} = parsedate($self->{PKT_TIME_FIRST});
      $self->{PKT_TIME_LAST} = parsedate($self->{PKT_TIME_LAST});
  }

  return;
}


1; # So require or use strict works
